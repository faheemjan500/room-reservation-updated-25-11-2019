var app = angular.module('app', []);

app.controller("menuCtrl", function ($scope, $http) {

    //$scope.showPersons = false;
    //let showRooms = false;
    // let showPeople = false;
    //let showStanze = false;
    $scope.showAllPersons = false;
    $scope.showAllRooms = false;


    $http({
        method: "GET",
        url: "http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/personJpa/getAllPersonData"
    }).then(function onSuccess(response) {
        $scope.personsFromServer = [];
        $scope.personsFromServer.push(response.data[0]);
        $scope.personsFromServer.push(response.data[1]);
        $scope.personsFromServer.push(response.data[2]);
        // for (var i = 0; i < response.length; i++) {
        //  $scope.list.push(response.data[i]);
        //}
        //$scope.persons = response.data;

    },
        function error(response) {
            $scope.person = response.statusText;
            alert("error getting all persons data")
        });

    $scope.deletePerson = function (key) {
        $http({
            method: "DELETE",
            url: ("http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/person/deletePerson/" + key)
        }).then(function onSuccess() {
            alert("person has been deleted");
        }, function error() {
            alert("error deleting person!");
        });
    };
    $scope.showPersons = function () {
        $scope.showAllPersons = !$scope.showAllPersons;
        $scope.showAllRooms = false;
        $scope.persons = [];
        $scope.persons = $scope.personsFromServer;
    }
    $scope.showInfo = function (person) {
        $scope.firstName = person.firstName;
        $scope.lastName = person.lastName;
        $scope.key = person.key;

        person.show = !person.show;
    }

    $http({
        method: "GET",
        url: "http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/getAllRoomData"
    }).then(function onSuccess(response) {
        $scope.roomsFromServer = [];
        $scope.roomsFromServer.push(response.data[0]);
        $scope.roomsFromServer.push(response.data[1]);
         $scope.roomsFromServer.push(response.data[2]);

       // for (var i = 0; i < response.length; i++) {
         //   $scope.roomsFromServer.push(response.data[i]);
        //}
        //$scope.rooms = response.data;

    },
        function error(response) {
            $scope.room = response.statusText;
            alert("error getting room data")
        });


    $scope.showRooms = function () {
        $scope.showAllRooms = !$scope.showAllRooms;
        $scope.showAllPersons = false;
        $scope.rooms = [];
        $scope.rooms = $scope.roomsFromServer;


    }
    $scope.deleteRoom = function (key) {
        $http({
            method: "DELETE",
            url: ("http://localhost:8080/meeting-room-reservation-app-1.0-SNAPSHOT/rest/room/deleteRoom/" + key)
        }).then(function onSuccess() {
            alert("room has been deleted");
        }, function error() {
            alert("error deleting room!");
        });
    };


    $scope.showInfo = function (room) {
        $scope.roomName = room.roomName;
        $scope.roomSize = room.roomSize;
        $scope.key = room.key;

        room.show = !room.show;
    }


})
//  app.config(['$routeProvider',

//      function($routeProvider){
//      $routeProvider
//      .when("/persondetails",{
//          templateUrl : "/personsdetails.html",
//         //  controller: "displayPersonsCtrl"
//          controller: "personCtrl"
//      })
//      .when("/roomdetails", {
//          templateUrl: "/roomdetails.html",
//          //controller: "displayRoomsCtrl"
//          controller: "roomCtrl"
//      })
//  }]);

//  app.controller("displayPersonsCtrl",function($scope){

//     $scope.message = "hello persons";

// });
// app.controller("displayRoomsCtrl",function($scope){

//     $scope.message = "hello rooms";
// });